---
title: Snowflake Engineering Challenges
date: October 25, 2022
institute: Brave Reading Group
author:
  - name: Cecylia Bocovich
    email: cohosh@torproject.org
slides:
    aspect-ratio: 169
    font-size: 14pt
    table-of-contents: false
---

## About Me

\begin{columns}
    \begin{column}{0.65\textwidth}
        \begin{itemize}
            \item Developer at The Tor Project since early 2018.
        \end{itemize}
    \end{column}

    \begin{column}{0.35\textwidth}
        \begin{center}
            \includegraphics[width=0.95\textwidth]{images/tor_man.png}
        \end{center}
    \end{column}
\end{columns}

## {.c .plain .noframenumbering}

\centering

This work is licensed under a

\large \href{https://creativecommons.org/licenses/by-sa/4.0/}{Creative Commons \\ Attribution-ShareAlike 4.0 International License}

\vfill

\ccbysa

\vfill
