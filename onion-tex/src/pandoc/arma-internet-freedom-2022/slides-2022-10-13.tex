%% Copyright (c) 2018 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.
\documentclass[aspectratio=169,14pt]{beamer}

\usepackage{soul,comment}
\usepackage{bbding,enumitem}

\usetheme{onion}

\title{October 2022 Internet Freedom RACE Follow-On}

\date{October 13 2022}
\author{Roger Dingledine}
\institute{The Tor Project}
\titlegraphic{\onionlogo{OnionBlack}}

\setlength{\parskip}{3mm}

% put page numbers on each slide
\setbeamertemplate{footline}{\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertframenumber}}}\hspace*{5pt}}

\begin{document}

\maketitle

\begin{frame}{Outline}

\begin{itemize}
\item {\bf{(0) Tor's track record in Internet Freedom}}
\item (1) Understand where we are censored and react to it
\item (2) Strengthen Snowflake
\item (3) Make our tooling more agile
\end{itemize}

\end{frame}

%%%%%%% (0) Tor's track record in Internet Freedom

\begin{frame}{Track record in Internet Freedom (1/3)}

Leading circumvention innovation
\vspace{-.15in}
\begin{itemize}[label=\textbullet]
\item \small Tor developed the concept of Pluggable Transports in 2010. Now, PTs are used in major circumvention tools and are their own field of research.
\vspace{-.05in}
\end{itemize}

Leading privacy innovation
\vspace{-.15in}
\begin{itemize}[label=\textbullet]
\item \small Features born in Tor Browser (like ``first-party isolation'') have been adopted as standard features in browsers like Firefox, Safari, and Brave.
\vspace{-.05in}
\end{itemize}

Building a research community
\vspace{-.15in}
\begin{itemize}[label=\textbullet]
\item \small Tor people helped found Privacy Enhancing Technologies
Symposium (PETS) and Free and Open Communications on the Internet (FOCI)
to foster research in privacy and censorship circumvention.
\vspace{-.05in}
\end{itemize}

\end{frame}

\begin{frame}{Track record in Internet Freedom (2/3)}

Legacy of real-world success
\vspace{-.15in}
\begin{itemize}[label=\textbullet]
\item \small From helping people stay connected during the Arab Spring
uprisings to supporting Russians to route around government censorship.
\vspace{-.05in}
\end{itemize}

Adoption by major players
\vspace{-.15in}
\begin{itemize}[label=\textbullet]
\item \small Leading social media and news outlets use Tor to help their
users circumvent censorship and to communicate safely with sources.
% [Could add logos]
\vspace{-.05in}
\end{itemize}

Invested community
\vspace{-.15in}
\begin{itemize}[label=\textbullet]
\item \small More than 80,000 volunteers have started Snowflake proxies
in the last 30 days, almost tripling the number of proxies in the network.
\end{itemize}

\end{frame}

\begin{frame}{Track record in Internet Freedom (3/3)}

Recognition
\vspace{-.15in}
\begin{itemize}[label=\textbullet]
\item \small Levchin Prize for Real-World Cryptography (2021)
\vspace{-.05in}
\item \small USENIX Test of Time Award (2014)
\vspace{-.05in}
\item \small EFF Pioneer Award (2012)
\vspace{-.05in}
\item \small Free Software Foundation Social Benefit Award (2011)
\vspace{-.05in}
\end{itemize}

Supporting an ecosystem / critical infrastructure
\vspace{-.15in}
\begin{itemize}[label=\textbullet]
\item \small A large ecosystem of tools rely on Tor to provide circumvention
and privacy options to their users.
\item \small We host subgrantees in our funded projects, to
build an ecosystem of collaborating circumvention tools and strong
organizations.
\end{itemize}

%\vspace{-.15in}
%\begin{itemize}[label=\textbullet]
%\item \small A large ecosystem of tools use Tor to provide circumvention
%and privacy options to their users.
%\end{itemize}

Always free software, always open source.
Since 2003.

\end{frame}

% Largest, most widely used network of its kind

% Rigorous and continuous validation

% Leading censorship circumvention and privacy innovation

\begin{frame}{Objectives}

\begin{itemize}
\item (0) Tor's track record in Internet Freedom
\item {\bf{(1) Understand where we are censored and react to it}}
\item (2) Strengthen Snowflake
\item (3) Make our tooling more agile
\end{itemize}

\end{frame}

%%%%%%% (1) Understand where we are censored and react to it

\begin{frame}{The bridge ``subscription'' model (1/3)}

%1a. The bridge "subscription" model: https://gitlab.torproject.org/tpo/anti-censorship/team/-/issues/42

We distribute bridges in many ways today (https, gmail, telegram, moat).

Each way has some different barrier to access, which aims to slow bridge
enumeration.

But bridges go down, or get blocked, or they're on dynamic IP addresses
and they just move, so users need to ask for new ones.

The key insight is: the barrier-to-access can be different for follow-on
requests.

\end{frame}

\begin{frame}{The bridge ``subscription'' model (2/3)}

Step one: flexible new protocol between Tor Browser and our back-end
bridge service:

\begin{itemize}
\item $\Rightarrow$ (1) client presents something $\Rightarrow$
\item
\begin{flushright}
$\Leftarrow$ (2) service replies with bridge info $\Leftarrow$
\end{flushright}
\item (3) Tor Browser auto adjusts our bridge list
\end{itemize}

%(can be interactive, via the same signaling channel used by Snowflake,
%Conjure, moat, etc)
Conceptually, this extends Tor Browser's ``moat'' design that fetches
new bridges + presents a Captcha in-browser.

\end{frame}

\begin{frame}{The bridge ``subscription'' model (3/3)}

The subscription model is a key building block for future plans:

\begin{itemize}[label=\textbullet]
\item auto replace bridges that go down
\item fast flux bridges that get blocked
\item reputation-based bridge distribution schemes like Salmon
\end{itemize}

%What the client sends will vary by use case: can send hash of bridge
%fingerprint to prove knowledge of bridge, or can send Salmon proof of
%identity, etc

For first two, client sends hash-of-bridge-fingerprint to prove knowledge
of bridge. For Salmon/Lox, it's a cryptographic identity.

\end{frame}

\begin{frame}{How to choose a replacement bridge?}

Easy: bridge moves to a new IP address $\Rightarrow$ give users its new info.

Easy: bridge goes offline $\Rightarrow$ give users a deterministic replacement.

Harder: bridge gets blocked.\\
%
%Because in the censorship arms race,
%Auto-replacing censored bridges means the censor
%auto-gets the new ones too!
Auto-replacing means the censor
auto-gets the new one too!

Whether to auto-replace blocked bridges depends on context: how dynamic
are our bridges vs how quick is the censor?

\end{frame}

%show china blocking timeframe cdf graph

\begin{frame}{Public bridges blocked in China in days to weeks}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{chinablockcdf.png}
\end{figure}
\end{frame}

\begin{frame}{Strategies for choosing a replacement bridge}

Reconcile the contradiction by having
two different classes of bridges---volunteer ones and centrally
managed ones. Can cycle the managed ones quickly, to play a faster
arms race with them.

Remaining agile here is key: we will start with a simple plan for *how*
we will choose a replacement bridge, and then we will learn from our
deployment and adapt.

\end{frame}

%\begin{frame}{Using bridge reachability vantage points}

%In the beginning, the idea was that we would look at self-reported stats
%from the bridges to decide which ones are still reachable.

%But the pace of the arms race is much faster now: bridges are blocked
%from China in days to weeks.

%\end{frame}

\begin{frame}{Using bridge reachability vantage points}

Over the past months we've been doing active bridge reachability
measurements from three countries: China, Russia, Turkey.

Privacy-preserving daily snapshots are published on
{\url{https://gitlab.torproject.org/tpo/anti-censorship/connectivity-measurement/bridgestatus}}

With the ``vantage point'', ``subscription model'', ``dynamic
cloud bridges'', and ``signaling channel'' building blocks,
the next step is to complete the feedback loop: we need to auto notice that
bridges have been blocked, and auto replace the bridge for the user.

\end{frame}

\begin{frame}{Refinements at the client side}

When exactly should Tor Browser launch a bridge refresh attempt?

Every time it fails to bootstrap? Periodically?

What about when bridges fail after bootstrap? How to distinguish from
simple network failure.

%We need to automatize things here for faster and more reproducible rollout

\end{frame}

\begin{frame}{Refinements at the scanning side}

We want to scan failed bridges sooner---so we can get working bridges
to users asap.

We want to scan working bridges less often---so we expose less info at
our vantage points.

Long term vision: use heuristics for prioritizing which bridges to
test from our sensitive vantage points, e.g. observing a drop in
usage or a spike in users requesting a replacement for it.

Scanning for `down' is a different race than scanning for `blocked', but
getting really good at the former can help the latter.
%noticing down means less need for risky testing.

%This way we don't test bridges inside China until it's likely that they
%have been blocked---it is to reduce the risk from testing bridges and
%having the test itself cause blocking.

\end{frame}

\begin{frame}{rdsys + bridgestatus + bridgestrap integration}

Strengthen the feedback loop between rdsys and the vantage points.

Need to expand the vantage points to more countries.

\end{frame}

\begin{frame}{Objectives}

\begin{itemize}
\item (0) Tor's track record in Internet Freedom
\item (1) Understand where we are censored and react to it
\item {\bf{(2) Strengthen Snowflake}}
\item (3) Make our tooling more agile
\end{itemize}

\end{frame}

\begin{frame}{Better understand Snowflake use and bottlenecks}

Initial goal: Make Snowflake user counts more accurate.

Right now we mainly count Snowflake users via self-reported ``consensus
fetch'' counts at the bridge.

But counting by consensus fetch undercounts by as much as 10x.

Worse, our ``many bridges, one fingerprint'' scalability band-aid
exposed bugs in the metrics portal: another 12x undercount!

\end{frame}

% a graph of what relay-search thinks flakey is
% doing, vs a graph of how many total snowflake users we think there are,
% to juxtapose them and make the difference visually clear:

\begin{frame}{Snowflake activity reported by metrics portal}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{screenshot-2022-10-11-flakey4.png}
\end{figure}
\end{frame}

\begin{frame}{Snowflake activity reported by back-end server}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{snowflake-procnetdev-bw-20221006.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=.6\textwidth]{screenshot-twitter-iran.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=1.0\textwidth]{screenshot-google-app-store-iran.png}
\end{figure}
\end{frame}


\begin{frame}{Instrument each layer to better understand usage}

We have other sources of information:

\begin{itemize}[label=\textbullet]
\item the broker knows how many requests, how many proxies, how many matching
attempts
\item the snowflake back-end knows how many sessions
\end{itemize}

%Getting stats at the broker side also lets us count how many *attempted*
%users there are, which is different from how many people make it all the
%way to the snowflake bridge.
The broker can count connection attempts, different from
successful users.

Especially important to be able to watch these stats as we make more
architectural changes e.g. multiple snowflake back-ends.

\end{frame}

% would be good also get:
%<diagram of snowflake users, broker, proxies, backends, bridges>
% in order to visually show the bottlenecks and what needs to be scaled

\begin{frame}{Improve Snowflake's denial-of-service resilience}

Make and implement a roadmap for ensuring Snowflake
keeps working in the future---even with increased scrutiny.

Understand the scope of the problem + design mitigations.

Two focuses:

\begin{itemize}[label=\textbullet]

\item Redundancy of components (e.g. deploy multiple brokers, multiple
bridges, multiple proxy pools)

\item Agility at detecting and handling protocol-level attacks
(e.g. somebody pretends to be a million snowflake users and uses up all
our Snowflakes).

\end{itemize}

\end{frame}

\begin{frame}{Objectives}

\begin{itemize}
\item (0) Tor's track record in Internet Freedom
\item (1) Understand where we are censored and react to it
\item (2) Strengthen Snowflake
\item {\bf{(3) Make our tooling more agile}}
\end{itemize}

\end{frame}

\begin{frame}{We're replacing BridgeDB with rdsys}

BridgeDB has grown organically since we first wrote it in 2007.

At this point, maintenance by new developers is a constant source of surprises.
The last change broke translations when it was an unrelated change.

%It takes a lot of effort to do changes, it's dragging things down for every small modification that we want to do.

We especially need agility for adjusting bridge assignments, e.g. if we
find that China is doing something new around IP ranges and we want to
change which bridges we give out via moat.

A refresh redesign, named rdsys, is making it better.

\end{frame}

%\begin{frame}{Some components in Snowflake are still proofs-of-concept}
\begin{frame}{Some Snowflake pieces are still proofs-of-concept}

The broker aims to be simple (just match users to proxies); some users
not being matchable to some proxies was not expected.

We want to expand to multiple proxy pools, multiple brokers, multiple
back-ends, load balance better across proxies.

Eventually we will want to keep track of which proxies work well
in which countries, and the matching constraints will get even more complex.

\end{frame}

\begin{frame}{Making the distributors agile again}

Two focuses here:

(A) redesign the bridge assignment / snowflake assignment code so
it is easy to add or change constraints.

(B) staging environments, sandboxes, and continuous integration, for
testing changes in each of these services.

These staging / sandbox environments will let us experiment and
try out new ideas without putting the production services at risk.

%Lack of a staging environment for the bridge distribution service means that bugs in dev environment show up in the production environment. The process for *testing* changes is not great. Separating staging from production will let us experiment more efficiently and let us be agile in trying out new ideas. Need to setup CI, setup servers that can automatically build from the CI.

%Having a staging environment for Snowflake---at each commit, we deploy a full Snowflake and see that it works.

\end{frame}

\begin{frame}{Bringing it all together}

(1) Understand where we are censored and react to it
\vspace{-.1in}
\begin{itemize}[label=\textbullet]
\item design and implement subscription model
\item use it to auto-replace broken bridges for Tor Browser users
\end{itemize}

(2) Strengthen Snowflake
\vspace{-.1in}
\begin{itemize}[label=\textbullet]
\item instrument each layer to better understand usage
\item redundancy of components, look at protocol-level attacks
\end{itemize}

(3) Make our tooling more agile
\vspace{-.1in}
\begin{itemize}[label=\textbullet]
\item redesign the bridge assignment / snowflake assignment code
\item staging (sandbox) environments and continuous integration
\end{itemize}

\end{frame}

\end{document}

