%% Copyright (c) 2018 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.
\documentclass[aspectratio=169,17pt]{beamer}

\usepackage{soul,comment}
\usepackage{bbding,enumitem}

\usepackage{tikzpeople}
\usepackage{pgfplots}
\usepackage{xcolor}
\usepackage{ulem}

\usetheme{onion}

\title{How Russia is trying to block Tor}

\date{March 15 2023}
\author{Roger Dingledine}
\institute{The Tor Project}
\titlegraphic{\onionlogo{OnionBlack}}

\setlength{\parskip}{3mm}

% put page numbers on each slide
\setbeamertemplate{footline}{\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertframenumber}}}\hspace*{5pt}}

%% AHF: \tikzexternalize uses the external library to cache tikz images, but needs --shell-escape to be passed to pdflatex.
%\usetikzlibrary{shapes,backgrounds,dateplot,external}
\usetikzlibrary{shapes,backgrounds,dateplot,external,lindenmayersystems}
%%\tikzexternalize[prefix=tikz/]

\newcommand{\highlight}[1]{\textbf{\alert{#1}}}

\newcommand{\colorhref}[3][OnionBlack]{\href{#2}{\color{#1}{#3}} }

\begin{document}

\maketitle

\begin{frame}{Outline}

\begin{itemize}
\item {\bf{(1) Intro to Tor}}
\item (2) Intro to Tor and censorship resistance
\item (3) What Russia did, when, responses
\item (4) Bigger context in Russia
\item (5) Better solutions
\item (6) What's next
\end{itemize}

\end{frame}

%%%%%%% (1) Intro to Tor

\begin{frame}{Tor Overview}

%Tor provides
Online anonymity: open source, open network

Community of devs, researchers, users, relay operators

US 501(c)(3) non-profit organization with 30ish staff

Estimated 2,000,000 to 8,000,000 daily users

Part of larger ecosystems: internet freedom, free software, censorship
resistance, anonymity research

\end{frame}

\begin{frame}{History}
\protect\hypertarget{history}{}
\begin{description}
%\item[\textbf{1990s}]
%Onion routing for privacy online.
\item[\textbf{2002--}]
Research with U.S.~Naval Research Lab.
\item[\textbf{2004}]
Sponsorship by Electronic Frontier Foundation.
\item[\textbf{2006}]
The Tor Project, Inc.~became a non-profit.
\item[\textbf{2007}]
\highlight{Expansion to anti-censorship.}
\item[\textbf{2008}]
Tor Browser development.
\item[\textbf{2010}]
The Arab spring.
\item[\textbf{2013}]
The summer of Snowden.
\item[\textbf{2018}]
\highlight{Dedicated anti-censorship team created.}
\end{description}
\end{frame}

\input{threat-model.tex}

\begin{frame}{Communications Metadata}
\protect\hypertarget{metadata}{}
\centering

\includegraphics[width=0.6\textwidth]{images/michael_hayden.jpg}

\begin{minipage}[b]{0.7\textwidth}
    \begin{quote}
        \small{"We Kill People Based on Metadata."}

        \begin{flushright}
            \footnotesize{---Michael Hayden, former director, NSA.}
        \end{flushright}
    \end{quote}
\end{minipage}
\end{frame}

\input{anonymity-is.tex}

\input{three-hops.tex}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/bandwidth-2013-01-01-2023-03-04.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.8\textwidth]{images/tor-browser-115.png}
\end{figure}
\end{frame}

\begin{frame}{Transparency for Tor is key}

\begin{itemize}[label=\textbullet]
\item<1-> Open source / free software
\item<1-> Public design documents and specifications
\item<1-> Publicly identified developers
\item<2-> Not a contradiction: privacy is about choice!
\end{itemize}

\end{frame}

%%%%%%% (2) Crash course on censorship-resistance

\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item {\bf{(2) Intro to Tor and censorship resistance}}
\item (3) What Russia did, when, responses
\item (4) Bigger context in Russia
\item (5) Better solutions
\item (6) What's next
\end{itemize}

\end{frame}

\begin{frame}{Bridges, for IP address blocking}

The Tor network is made up of 7000 public relays, but the list is public
and you can just fetch it and block them by IP address.

So, we have unlisted relays called ``bridges'' and there is a
cat-and-mouse game where users try to get a bridge that the censor didn't
already find.

\end{frame}

\begin{frame}{Pluggable transports, for DPI blocking}

%At the same time there is a \emph{protocol-level} arms race: the original
%Tor protocol looks mostly like TLS, but there are distinguishers if you
%look carefully enough.
{\it{Protocol-level}} arms race too: the Tor protocol looks mostly like
TLS, but only if you don't look too carefully.

%Rather than somehow perfectly mimicking a browser talking to a webserver,
%instead our ``pluggable transports'' design aims for modularity:
Rather than perfectly mimicking Firefox+Apache, our ``pluggable
transports'' design aims for modularity:

Tor's three-hop path provides the privacy, and
you can plug in different {\it{transports}} that transform the first
link's traffic into flows that your censor doesn't block.

\end{frame}

\begin{frame}{obfs4 pluggable transport}

obfs4 is still the core most successful transport.

It simply adds a new layer of encryption on top, so there are no
recognizable headers or formatting at the content layer.

The idea is that automated protocol classifiers won't have any good
guesses, so censors are forced to either block all unclassified traffic
or allow it all through.

\end{frame}

\begin{frame}{Snowflake pluggable transport}

Snowflake makes your traffic look like a WebRTC (zoom, jitsi, skype,
signal, etc) call, and those are allowed in many parts of the
world.

%Other key innovation is that 
People can volunteer as Snowflake
proxies simply by installing an extension in their browser.

The resulting volume and variety of volunteers gives us more options on
how to distribute them to users.

\end{frame}

\begin{frame}{meek pluggable transport}

%Tunnels traffic inside an https request to a shared cloud service
Domain fronting: Makes https request to a shared cloud service (azure,
fastly, etc), and tunnels traffic inside it

Outer layer says the SNI (Server Name Indicator) of a popular site, but
inner layer has a different Host: header

Have to pay cloud prices for the bandwidth :(, so not great for proxying
full traffic flows

\end{frame}

\begin{frame}{Matching bridges to users who need them}
%Distributors / brokers}

%Services for matching up bridges to users who need them.

Divide obfs4 bridges into distribution buckets, where each
bucket relies on a different scarce resource to rate-limit how many
bridges the censor can get.

\begin{itemize}[label=\textbullet]

\item https (get a few bridges based on your IPv4 /16)

\item gmail (get a few bridges based on your username)

\item moat (Tor Browser makes a domain-fronted connection, presents
a captcha in-browser, and auto-populates your bridge settings).

\end{itemize}

\end{frame}
\begin{frame}{Matching bridges to users who need them}

and Snowflake has a similar ``broker'' service that matches up Snowflake
users to Snowflake volunteers.

\end{frame}

%%%%%%% (3) Russia

\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item \sout{(2) Intro to Tor and censorship resistance}
\item {\bf{(3) What Russia did, when, responses}}
\item (4) Bigger context in Russia
\item (5) Better solutions
\item (6) What's next
\end{itemize}

\end{frame}

\begin{frame}{Russia, December 2021}

%It started with some ISPs blocking the public Tor relays, plus meek-azure
%domain fronting, plus default obfs4 and some moat-distributed obfs4
%bridges, plus the Snowflake protocol.
Some ISPs blocked (a) the public Tor relays, (b) meek-azure
domain fronting, (c) Tor Browser's default obfs4 bridges, (d) some
moat-distributed obfs4 bridges, (e) the Snowflake protocol.

A week later we got an actual legal notice that the Tor website was evil
and they were going to censor it. Then they did.

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/userstats-relay-country-ru-2021-12-01-2021-12-31-off.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/userstats-bridge-country-ru-2021-12-01-2021-12-31.png}
\end{figure}
\end{frame}

% dpi attack by russia on pion webrtc lib
%https://github.com/pion/dtls/issues/409
%https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40107
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-support-groups-dpi.png}
\end{figure}
\end{frame}

%not actually the way they blocked it, but we fixed this one anyway
%https://github.com/pion/dtls/pull/407
%\begin{frame}
%\begin{figure}[htbp]
%\includegraphics[width=\linewidth]{images/screenshot-sni-naked-ip-addresses.png}
%\end{figure}
%\end{frame}

%russia blocking meek front, by blackholing an entire azure cdn IP
%address (which served the rest of the region for all azure-hosted sites
%because geodns)
% https://gitlab.torproject.org/tpo/community/support/-/issues/40050#note_2765616
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-meek-azure-block.png}
\end{figure}
\end{frame}

\iffalse

%- better SNI choice than stackexchange for snowflake front?
%https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40068
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-sni-sstatic.png}
\end{figure}
\end{frame}

\fi

\begin{frame}{Russia, mid 2022}

%More aggressive blocking, now including other obfs4 distributors

Now Russia crawling all three legacy categories of bridgedb obfs4 bridges (moat, https, email)

But still not instantaneous: new bridges last days to weeks

Other obfs4 bridges still work fine

Snowflake and meek continue to work, but are slower

\end{frame}

%- and russian language support
%  - https://forum.torproject.net/t/tor-blocked-in-russia-how-to-circumvent-censorship/982
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-forum-post.png}
\end{figure}
\end{frame}
\begin{frame}{177k views!}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-forum-post2b.png}
\end{figure}
\end{frame}
%  - rapid response grant by OTF, another one coming from DDF
%  - (.net addresses not blocked so much)

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/userstats-relay-country-2021-06-10-2023-03-10-country-ru.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/userstats-bridge-combined-2021-06-10-2023-03-10-country-ru.png}
\end{figure}
\end{frame}

%- remember that our stats are a conservative (probable under)estimate.
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-user-count-ticket.png}
\end{figure}
\end{frame}

%- remember that our stats are a conservative (probable under)estimate.
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/user-count-bounds.png}
\end{figure}
\end{frame}

% growth in snowflake population and snowflake usage
\begin{frame}
\vspace{-1cm}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/snowflake-totals.png}
\end{figure}
\end{frame}

\begin{frame}
\vspace{-1cm}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/snowflakes-2021-11-2023-03.png}
\end{figure}
\end{frame}

%  - graphs of each
%    - including a corrected graph which shows actual flakey user count + load
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/snowflake-big-picture.png}
\end{figure}
\end{frame}

%- the scope and scale of new obfs4 bridges too
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/networksize-2021-10-01-2022-07-22.png}
\end{figure}
\end{frame}

%- telegram-based bridge distribution, with a twist ***
\begin{frame}{Telegram-based bridge distribution}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-forum-telegram.png}
\end{figure}
\end{frame}

%\begin{frame}{9k requests/day, peaking close to 20k/day}
%\begin{figure}[htbp]
%\includegraphics[width=\linewidth]{images/telegram_req_day.png}
%\end{figure}
%\end{frame}

\begin{frame}{A popular telegram-distributed obfs4 bridge}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-telegram-bridge.png}
\end{figure}
\end{frame}

\begin{frame}{Insider information}

<redacted>

\end{frame}

\begin{frame}{Telegram lessons}

Other bridge distribution techniques would complement well, e.g. ``Use rotating time periods for when we distribute each bridge''
%  - https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/79

Captcha as a technique is increasingly broken and also increasingly burdensome

Reputation-based bridge distribution designs look well suited for the Russia situation

But still some hard open problems, e.g. sybil resistance

Many of these open questions are going to need to get tackled by the snowflake broker too, as the arms race wears on

\end{frame}

\begin{frame}{New OONI Snowflake test}
\begin{itemize}[label=\textbullet]
\item {\url{https://explorer.ooni.org/experimental/mat?probe_cc=CN&test_name=torsf&since=2022-02-07&until=2022-03-10&axis_x=measurement_start_day}}
\item {\url{https://github.com/ooni/probe/issues/2004}}
\end{itemize}
\end{frame}

%- and ooni tests of tor in general
%https://github.com/ooni/probe/issues/1730

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-ooni-torsf-1b.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-ooni-torsf-2c.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-40062.png}
\end{figure}
\end{frame}

\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item \sout{(2) Intro to Tor and censorship resistance}
\item \sout{(3) What Russia did, when, responses}
\item {\bf{(4) Bigger context in Russia}}
\item (5) Better solutions
\item (6) What's next
\end{itemize}

\end{frame}

%%%%%%% meanwhile...

%- and fighting the russian censorship law in their courts, courtesy an ngo
\begin{frame}{Challenging the censorship legally}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-appeal-ru.png}
\end{figure}
\end{frame}
\begin{frame}{Challenging the censorship legally}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-appeal.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-blocking-overturned.png}
\end{figure}
\end{frame}

%- Roskomnadzor leak
%  http://ddosxlvzzow7scc7egy75gpke54hgbg2frahxzaw6qq5osnzm7wistid.onion/wiki/Roskomnadzor
\begin{frame}{Censorship authority dox}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-leak.png}
\end{figure}
\end{frame}

\begin{frame}{Other surprises}

\begin{itemize}[label=\textbullet]

\item rt.com censored from many Tor exits, because Europe (i.e. France,
part of Germany, maybe more now?) pledged to censor it.

\item Actually, many Tor exits can't reach sites in Russia now, because
the blocking is bidirectional?

\item New groups of Russian and Ukrainian exit relays, ``hmm''

%\item I'm talking to a contact at Amazon about pitching turning back on domain fronting... for Ukraine

\end{itemize}

\end{frame}

\iffalse
\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/unbound-flood.png}
\end{figure}
\end{frame}
\fi

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/snowflake-locations2.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/userstats-relay-country-ua-2021-12-24-2022-03-24-off.png}
\end{figure}
\end{frame}

\begin{frame}{Down the road of the arms race (1/2)}

Consider {\it{user impact}} from the censorship arms race.

Yes, we have steps to counter each step the censor takes. But as we
move down that path, users have a higher burden to achieve
a working connection.

\end{frame}

\begin{frame}{Down the road of the arms race (2/2)}

We see this user drop-off effect already, where the number of Tor users
who switched over to bridges is impressive, but it's definitely not all
of them.

We need to find ways to reduce that burden, and/or slow down the arms
race, else the censor wins because the average user won't care enough
to bother.

\end{frame}


\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item \sout{(2) Intro to Tor and censorship resistance}
\item \sout{(3) What Russia did, when, responses}
\item \sout{(4) Bigger context in Russia}
\item {\bf{(5) Better solutions}}
\item (6) What's next
\end{itemize}

\end{frame}

\begin{frame}{Building block 1}

\#1: a DPI-resistant point-to-point channel.

That's obfs4, but it can also be v2ray's vmess or any similar project.

\end{frame}

\begin{frame}{Building block 2}

\#2: a Sybil-resistant sign-up mechanism.

%moat/https/gmail buckets are a start, but account-age-based
%Telegram distributor is a better start.
moat/https/gmail buckets are a good start, but
Telegram distributor (with twist) is a better start.

Need to identify resources where (a) individual users probably have a
few but it's tough for the censor to have many, and
(b) it's easy to verify in an automated way.

Think ``old Facebook accounts'' or ``friends on Twitter''

\end{frame}

\begin{frame}{Building block 3}

\#3: a large pool of diverse bridge addresses.

There are
two pieces to this: one is the group of thousands of volunteers who
run their own bridges.

The second is an automated framework for easily
spinning up and down bridge images on popular cloud providers.

\end{frame}

\begin{frame}{Dynamic bridges, easy to rotate}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-sr2-bridges.png}
\end{figure}
\end{frame}
%- irl's dynamic bridge set-up, where it's easy to rotate a bridge ***
%  - https://gitlab.com/sr2c/bridge-dashboard/
%  - https://gitlab.com/sr2c/docker-bridgestrap

\begin{frame}{Building block 4}

\#4: a robust signaling channel.

Need some
bidirectional channel that will reliably work, i.e.~that censors won't
want to block even when they know the details of how it works.

It's fine if it's low-bandwidth and not-great-latency.

Domain fronting through Fastly; proxy through Google AMP cache; tunnel
through DoH or email.

\end{frame}

\begin{frame}{Building block 5}

\#5: a way to establish ground-truth about a suspected-blocked bridge.

\end{frame}

\begin{frame}{bridgestrap ``obfs4 reachability'' tool}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-bridgestrap.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-bridgestrap-detail.png}
\end{figure}
\end{frame}

%\begin{frame}
%\begin{figure}[htbp]
%\includegraphics[width=\linewidth]{images/screenshot-bridgestatus.png}
%\end{figure}
%\end{frame}

\begin{frame}{Centralized probe log collection}

\begin{itemize}[label=\textbullet]

\item {\url{https://gitlab.torproject.org/tpo/anti-censorship/team/-/issues/54}}

\item First vantage points in China, Turkey, Russia, Ukraine

\item Emphasis on more frequent testing because partial (slow) bootstrap
results are more common than we first expected

%  - plus a temporary "control" vantage point in ireland, but hopefully not for long
%\item Upcoming step: link these probe results to the dynamic bridge tools, and then the automation really starts to come together

\end{itemize}

\end{frame}


\begin{frame}{Milestone 1: Tor Browser automation}

{\it{Tor Browser uses the signaling channel to learn the recommendations
for your country.}}

It fetches a json file with an ordered list of which pluggable transports
and which bridge distribution strategies are expected to work best in
each country.

Then Tor Browser walks the user through trying them.

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-json-censorship-map.png}
\end{figure}
\end{frame}

% Tor Browser 11.5's automatic censorship handling

\iffalse

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{auto/connection-assist-connection-error_2x.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{auto/connection-assist-location-settings_2x.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{auto/connection-assist-connecting_2x.png}
\end{figure}
\end{frame}

\fi

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{auto/connection-assist-bridge-error_2x.png}
\end{figure}
\end{frame}

\iffalse

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{auto/connection-assist-last-attempt_2x.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{auto/connection-assist-final-error_2x.png}
\end{figure}
\end{frame}

\fi

\begin{frame}
\vspace{-2cm}
\begin{figure}[htbp]
\includegraphics[width=9cm]{auto/660-768-max.png}
\end{figure}
\end{frame}

\iffalse

\begin{frame}
\vspace{-4cm}
\begin{figure}[htbp]
\includegraphics[width=9cm]{auto/connected-builtin-obfs4-collapsed_2x.png}
\end{figure}
\end{frame}

\begin{frame}
\vspace{-2cm}
\begin{figure}[htbp]
\includegraphics[width=9cm]{auto/connected-builtin-obfs4-qr-zoom_2x.png}
\end{figure}
\end{frame}

\fi

\begin{frame}{Milestone 2: Bridge ``subscription'' model}

Short-lived bridges are a reliability problem, separate from
censorship.

With Tor Browser automation, next step use the signaling channel to
ask for a replacement bridge.

You use the old bridge as the credential to prove that you
deserve a new one---skipping the proof-of-resource step so it
can all happen in the background.

\end{frame}

\begin{frame}{Dynamic cloud bridges}

The subscription model makes the dynamic cloud bridge approach much more
powerful: if a bridge gets blocked anywhere, you can spin it down
and spin up a new one somewhere else, and users will be able to follow
their bridge.

But there is a tradeoff: censors can block a bridge they know and then
follow it and block its replacement, etc.

\end{frame}

\begin{frame}{Milestone 3: Trust-based distribution (1/2)}

(designs like Salmon, rBridge, Hyphae, Lox)

%Once we have Tor Browser doing
%automated background connections to maintain your bridges, the next
%step is rather than just replacing bridges when they die, instead track
%whether the bridges you gave somebody got blocked.
With Tor Browser doing
background connections to maintain your bridges, now
rather than just replacing bridges when they die, instead track
whether the bridges you gave somebody got blocked.

Get rid of users whose bridges tend to get blocked, and reward users
whose bridges last a long time.

\end{frame}

\begin{frame}{Milestone 3: Trust-based distribution (2/2)}

%Many open problems remain here.

Need to pick the right parameters so good users get enough bridges yet
bad people can't find too many.

%Salmon lets trusted users invite friends
%to higher trust levels; if a user misbehaves blame their
%sponsor too. But now we need a privacy-preserving social graph
%management mechanism?
Salmon lets trusted users invite friends
to higher trust levels; if a user misbehaves blame their
sponsor too. But...privacy-preserving social graph management?

%Despite these challenges, I think 
This is where the arms race needs to go:
focus on {\it{exploiting asymmetries}} against well-funded censors.

\end{frame}

\begin{frame}{Outline}

\begin{itemize}
\item \sout{(1) Intro to Tor}
\item \sout{(2) Intro to Tor and censorship resistance}
\item \sout{(3) What Russia did, when, responses}
\item \sout{(4) Bigger context in Russia}
\item \sout{(5) Better solutions}
\item {\bf{(6) What's next}}
\end{itemize}

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/russia-sanctions.png}
\end{figure}
\end{frame}

\begin{frame}{First, a rant about sanctions}

Especially about hurting internet connectivity for people in Russia as
a way to punish their government.

Compare to the effects of Trump's ``maximum pressure'' sanctions
against Iran.

We will see the same outcome in Russia.

\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=\linewidth]{images/screenshot-rt.png}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.7\textwidth]{images/jealous-meme.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.6\textwidth]{images/offramp-meme.jpg}
\end{figure}
\end{frame}

\begin{frame}
\begin{figure}[htbp]
\includegraphics[width=0.5\textwidth]{images/office-meme.jpg}
\end{figure}
\end{frame}

\begin{frame}

\huge{\textcolor{red}{WTF Europe?? Why u like censoring so much??}}

\end{frame}

\begin{frame}{Internet censorship: early warning system}

Notice that our Russia Tor blocking story started at the beginning of
December 2021.

From the rest of the world's perspective, the Russia story started in
February 2022.

So (a) yeah they knew this was coming, and (b) internet censorship often
serves as an early warning system for upcoming political events.

\end{frame}

\begin{frame}{GFW blocking by too much entropy}

China is experimenting with blocking based on flow-level entropy: if
the first k characters in the flow are too random, they kill it.

Only doing it to flows destined for certain cheapo providers like Hetzner,
OVH, Digital Ocean, Alibaba.

Maybe they can't tolerate the collateral damage from doing it more
widely? Or, maybe they can?

We're going to need better transports.

\end{frame}

\begin{frame}{Calls to action}

Please run bridges!

Please run Snowflakes!

Please run relays!

Please participate in anti-censorship research!
https://foci.community/ attached to PETS in Lausanne in July.

\end{frame}

\end{document}

