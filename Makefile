TARGET     = slides.pdf
SOURCE     = slides.tex
SOURCES    = $(wildcard *.tex)

export TEXINPUTS:=$(shell pwd)/onion-tex/src/tex:${TEXINPUTS}

all: $(TARGET)

$(TARGET): tikz $(SOURCES) images/*
	latexmk -g -pdf -pdflatex="pdflatex --shell-escape %O %S" $(SOURCE)

data:
	mkdir -p data || true

tikz:
	mkdir -p tikz || true

clean:
	rm -fr $(TARGET_TEX) tikz/ *.vrb *.dvi *.pdf *.aux *.auxlock *.fdb_latexmk *.fls *.log *.nav *.out *.snm *.toc || true

.PHONY: clean
